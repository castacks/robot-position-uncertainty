classdef visualize_results < parameters
    properties
        y_limit=150;
    end
    methods
        function optimal_dist(obj)
            for i=1.1:0.2:2.5
                filename = ['results/2016_11_20/sim_', num2str(obj.lx),'_', num2str(obj.ly),'_dist_',num2str(i),'_avg.mat'];
                s=load(filename);
                figure(1);
                ylim([0,obj.y_limit]);
                title(['Multiple Runs with dist']);
                grid on;
                [m,ind]=min(s.data_check_avg(:,4))
                plot(i,m,'--gs','LineWidth',2,'MarkerSize',10,...
                'MarkerEdgeColor','b',...
                'MarkerFaceColor',[0.5,0.5,0.5]);
                xlabel('Distance From Wall');
                ylabel('Time for Inspection');
                hold on;     
            end
        end
        function show(obj)
            for i=1.1:0.2:2.5
                d=[];
                filename = ['results/2016_11_20/sim_', num2str(obj.lx),'_', num2str(obj.ly),'_dist_',num2str(i),'_avg.mat'];
                s=load(filename)
                if(~isempty(d))
                    d=(d+s.data_check_avg);
                else
                    d=s.data_check_avg;
                end
                figure (1);
                plot(d(:,1), d(:,4),'b');
                ylim([0,obj.y_limit]);
                title(['Multiple Runs with dist=',num2str(i)]);
                grid on;
                [m,ind]=min(d(:,4))
                hold on;
                plot(d(ind,1),m,'r*','Markersize',20);
                pdf_file=['results/2016_11_20/result_',num2str(i),'.mat'];
                s=singleRun(pdf_file);
                s.velcheck();
                hold on;
                plot(obj.v_min:1/obj.resol:obj.v_max,s.upper_bound_time,'g');
                [m,ind]=min(s.upper_bound_time);
                hold on;
                plot(s.result(ind,2),m,'k*','Markersize',20);
                hold on;
                plot(s.optimal_value(2),s.optimal_value(4),'g*','Markersize',20);%single run optimal value
                h_legend=legend('TSP Simulation','Optimal Value of TSP Sim','Upperbound','Optimal Value of Upperbound','Single Run Optimal Value');
                set(h_legend,'FontSize',20);
                set(gcf,'color','w');
                pause(1);
                close all;
            end
        end
        function single_run_visualize(obj)
            load('results/singleRun_sim/singleRun_sim_10_10_dist_2_avg.mat');
            results = 'results/2016_11_20/result_2.mat';
            obj = singleRun(results);
            res = obj.velcheck
            % [X,Y] = meshgrid(data_check_avg(:,1), data_check_avg(:,3));
            [X,Y] = meshgrid(0:0.1:5.2, 0:2:1000);
            Z = griddata(data_check_avg(:,1), data_check_avg(:,3), data_check_avg(:,4), X, Y);
            surf(X, Y, Z); 
            xlabel('velocity');ylabel('Time');zlabel('Uncovered Area');
            hold on;
            ind = find(data_check_avg(:,1) == min(data_check_avg(:,1)))
            h = scatter3(data_check_avg(ind,1), data_check_avg(ind,3), data_check_avg(ind,4), 'filled');
            h.SizeData = 100;
            text(data_check_avg(ind,1), data_check_avg(ind , 3) -1, data_check_avg(ind ,4)+2, 'Min Vel')

            ind = find(data_check_avg(:,1) == max(data_check_avg(:,1)));
            h = scatter3(data_check_avg(ind,1), data_check_avg(ind,3), data_check_avg(ind,4), 'filled');
            h.SizeData = 100;
            text(data_check_avg(ind,1) - 1, data_check_avg(ind,3) -1, data_check_avg(ind,4) + 2, 'Max Vel')
            % ind = find(data_check_avg(:,4) == min(data_check_avg(:,4)));
            % data_check_avg = data_check_avg(ind, :);
            % ind2 = find(data_check_avg(:,3) == min(data_check_avg(:,3)));
            ind2 = find(abs(data_check_avg(:,1)- res(2)) < 0.001);
            h = scatter3(data_check_avg(ind2,1), data_check_avg(ind2,3), data_check_avg(ind2,4), 'filled');
            h.SizeData = 100
            text(data_check_avg(ind2,1) + 1.5, data_check_avg(ind2,3) -1, data_check_avg(ind2,4) + 1, 'Optimal Vel')
        end
        function multiple_vs_single(obj)
            x1 = []; y1 = []; y2 = []; y3 = []; y4 = [];
            v1 = []; v2 = []; v3 = []; v4 = [];
            for i=1.1:0.2:2.3
                filename = ['results/2016_11_20/sim_', num2str(obj.lx),'_', num2str(obj.ly),'_dist_',num2str(i),'_avg.mat'];
                s=load(filename);
                figure(1);
%                 ylim([0,obj.y_limit]);
                title(['Multiple Runs vs Single Run']);
                grid on;
                [m,ind]=min(s.data_check_avg(:,4)); 
                x1 = [x1;i]; y1 = [y1;m];
                v1 = [v1;s.data_check_avg(ind, 2)];
                
                results = strcat('results/2016_11_20/result_', num2str(i), '.mat');
                obj = singleRun(results);
                obj.d = i;
                res = obj.velcheck;
                [m, ind] = min(obj.upper_bound_time);
                ind = find(abs(s.data_check_avg(:,1)- obj.result(ind,2)) < 0.001);
                y4 = [y4; s.data_check_avg(ind,4)];
                v2 = [v2;s.data_check_avg(ind, 2)];

                filename = ['results/singleRun_sim/singleRun_sim_', num2str(obj.lx),'_', num2str(obj.ly),'_dist_',num2str(i),'_avg.mat'];
                s=load(filename);
%                 figure(1);
%                 ylim([0,obj.y_limit]);
                grid on;
                ind = find(s.data_check_avg(:, 4) == min(s.data_check_avg(:,4)));
                [m, ind] = min(s.data_check_avg(ind,3));
                y2 = [y2;m];
                v3 = [v3;s.data_check_avg(ind, 1)];
                
                ind2 = find(abs(s.data_check_avg(:,1)- res(2)) < 0.001);
                y3 = [y3; s.data_check_avg(ind2, 3)];
                v4 = [v4;s.data_check_avg(ind2, 1)];
                
            end
            [x1, v1, v2, v3, v4]
            p = plot(x1, y1, x1, y2, x1, y3,  x1, y4,'LineWidth',2,'MarkerSize',10, 'MarkerFaceColor', [0.5,0.5,0.5]);
            p(1).Marker = 'o';
            p(1).Color = 'g';
            p(2).Marker = 'o';
            p(2).Color = 'b';
            p(3).Marker = 'o';
            p(3).Color = 'm';
            p(4).Marker = 'o';
            p(4).Color = 'y';
             
            hold off;  
            xlabel('Distance From Wall');
            ylabel('Time for Inspection');
            legend('Multiple Runs Simulation', 'Single Run Simulation', 'Single Runs Result', 'Multiple Runs Result');
        end
    end
end