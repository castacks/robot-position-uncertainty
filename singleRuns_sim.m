classdef singleRuns_sim < parameters
    properties
        max_trials=10;
    end
    methods
       function obj= singleRuns_sim(t,varargin)
           obj=obj@parameters(varargin{:})
           if nargin>0
               obj.max_trials=t;
           end
       end
       function tspMethod(obj)
            data_check_avg=[];
            for trials = 1:obj.max_trials
                data_check = [];
                for v1 = obj.v_min:0.1:obj.v_max
                    v2 = v1;
                    n=1;
                    x_shift=v1/obj.fps; y_shift=v2/obj.fps;
                    num_cells = ceil(obj.lx*obj.fps/v1) * ceil(obj.ly*obj.fps/v2);
%                     disp(['computing path for 1st run with v1 = ', num2str(v1),' and v2= ', num2str(v2)])
                    vec_x=[-x_shift/2:x_shift:(ceil(obj.lx*obj.fps/v1)+1)*x_shift];
                    vec_y=[-y_shift/2:y_shift:(ceil(obj.ly*obj.fps/v2)+1)*y_shift];                    
                    x_cor=repmat(vec_x,size(vec_y,2),1);   x_cor=x_cor(:);
                    y_cor=repmat(vec_y',size(vec_x,2),1);
                    path = [ones(size(x_cor)).*-obj.d, x_cor, y_cor];
                    time=[((num_cells-1)/obj.fps)];
                    vector = [1,0,0];
                    x1 = [0, 0, obj.lx, obj.lx, 0];
                    y1 = [0, obj.ly, obj.ly, 0, 0];
                    [second_pos, uncovered] =obj.uncovered_fov_mvn(path, x1, y1, v1, v2);
                    uncovered_area = 0;
                    if ~isempty(uncovered)
                        xd = uncovered(1,:);
                        yd = uncovered(2,:);
                        uncovered_area = obj.get_area(xd,yd);
                    end
                    disp(['Number of runs: ', num2str(n)])
                    disp(['Total time taken: ', num2str(sum(time))])
                    data_check = [data_check; double(v1),double(v2),double(sum(time)), double(uncovered_area)];
                    description = ['v1', '_v2','_time', '_uncovered_area'];
                end
                if(~isempty(data_check_avg))
                    data_check_avg=data_check_avg+data_check;
                else
                    data_check_avg=data_check;
                end
            end
            data_check_avg=data_check_avg./trials;
            filename = ['results/singleRun_sim/singleRun_sim_', num2str(obj.lx),'_', num2str(obj.ly),'_dist_',num2str(obj.d),'_avg','.mat'];
            save(filename, 'data_check_avg','description');
       end
       function [second_pos, uncovered]= uncovered_fov_mvn(obj,path, x1, y1, v1, v2)  
                n = size(path, 1);
                xd = x1;
                yd = y1;
                X = [];
                Y = [];
                second_pos=[];
                error = mvnrnd(obj.mu_e, obj.sigma_value, size(path,1));

                %Check for uncovered areas by repeatedly subtracting fov polygons from
                %entire wall
                for i = 1:size(path,1)
                    pos = [path(i,1)-error(i,1), path(i,2)-error(i,2), path(i,3)-error(i,3)];
            %         pos = [path.Poses(i).Position.X, path.Poses(i).Position.Y, path.Poses(i).Position.Z];
            %         [error(i,1), error(i,2), error(i,3), error(i,4)]
                    yaw=0;
                    yaw = yaw - error(i,4)*pi/180.0;
                    res = obj.get_fov(pos(1,2),pos(1,3), yaw, -pos(1,1), obj.f);
                    if (~isempty(res))
                        x2 = res(1,:);
                        y2 = res(2,:);
                        [xd, yd] = polybool('subtraction', xd, yd, x2, y2);
                    end
%                     X = [X;x2];
%                     Y = [Y;y2];

                end  

            uncovered = [xd;yd];

            uncovered_area = obj.get_area(xd,yd);
            disp(['Uncovered Area ', num2str(uncovered_area)])
            %     

            %     [f, v] = poly2fv(xd, yd);
            %     patch('Faces', f, 'Vertices', v, 'FaceColor', 'r','EdgeColor', 'none')
            %     axis equal, axis off, hold on
            %     for i = 1:size(X,1)
            %         plot(X(i,:), Y(i,:), 'Color', 'k')
            %     end
            %     plot(x1,y1)
            %     title('Uncovered area in red')

                if size(xd) == 0 
                    disp(['SUCCESS! Aborting'])
                    return
                end


            x_shift = v1/(obj.fps);
            y_shift = v1/(obj.fps);
            dist_from_wall = -pos(1,1);
            ver_wp = int32(ceil(obj.lx/y_shift));
            hor_wp = int32(ceil(obj.ly/x_shift));
            xcheck = xd;
            ycheck = yd;  

            while ~isempty(xd)
                ind = [0,find(isnan(xcheck)), size(xcheck,2)+1];
                for i = 1:size(ind,2)-1
                    x = xcheck(ind(i)+1:ind(i+1)-2);
                    y = ycheck(ind(i)+1:ind(i+1)-2);
                    nump = numel(x);
                    C = [(1:(nump-1))' (2:nump)'; nump 1];
                    dt = delaunayTriangulation(x',y',C);
                    io = dt.isInterior();
                    % Divide polygon into triangles if concave
                    if ~isempty(find(io==0))
                        i = dt(io,:);
                        for j= 1:size(i,1)
                            xn = [x(i(j,1)), x(i(j,2)),x(i(j,3))];
                            yn = [y(i(j,1)), y(i(j,2)),y(i(j,3))];
                            point_x = sum(xn)/size(xn,2);
                            point_y = sum(yn)/size(yn,2);
                            x2 = [point_x - x_shift/2, point_x - x_shift/2, point_x + x_shift/2,point_x + x_shift/2,point_x - x_shift/2];
                            y2 = [point_y - y_shift/2, point_y + y_shift/2, point_y + y_shift/2, point_y - y_shift/2, point_y - y_shift/2];
                            [xd, yd] = polybool('subtraction', xd, yd, x2,y2);
                            second_pos = [second_pos; -1.0 * dist_from_wall, point_x,point_y];
                        end
                    else
                        point_x = sum(x)/size(x,2);
                        point_y = sum(y)/size(y,2);
                        x2 = [point_x - x_shift/2, point_x - x_shift/2, point_x + x_shift/2,point_x + x_shift/2,point_x - x_shift/2];
                        y2 = [point_y - y_shift/2, point_y + y_shift/2, point_y + y_shift/2, point_y - y_shift/2, point_y - y_shift/2];
                        [xd, yd] = polybool('subtraction', xd, yd, x2,y2);
                        second_pos = [second_pos; -1.0 * dist_from_wall, point_x,point_y];
                    end
                end
            xcheck = xd;
            ycheck = yd;   
%             obj.get_area(xd,yd);
            end
       end
       function uncovered_area= get_area(obj,xd,yd)
        % Find area of uncovered regions
        uncovered_area = polyarea(xd,yd);
        if isnan(uncovered_area)
            ind = find(isnan(xd));
            ind = [ind, size(xd,2)+1];
            x = xd(1:ind(1)-1);
            y = yd(1:ind(1)-1);
            uncovered_area = polyarea(x,y);
            for i = 1:size(ind,2)-1
                x = xd(ind(i)+1 : ind(i+1)-1);
                y = yd(ind(i)+1 : ind(i+1)-1);
                if ispolycw(x,y)
                    uncovered_area = uncovered_area + polyarea(x,y);
                else
                    uncovered_area = uncovered_area - polyarea(x,y);
                end
            end
        end
       end
       function res = get_fov(obj,rx, ry, Y, d, f)
           dmax = 3; 
           if Y >= f | Y <= -f | (d>dmax)
                X = [];
                Y = [];
                res = [X;Y];
            else
                x1 = rx + d * tan(Y - f /2);
                y1 = ry + d * sec(-Y + f /2) * tan(f /2);
                x2 = rx + d * tan(Y + f /2);
                y2 = ry + d * sec(Y + f /2) * tan(f /2);
                x3 = rx + d * tan(Y - f /2);
                y3 = ry - d * sec(-Y + f /2) * tan(f /2);
                x4 = rx + d * tan(Y + f /2);
                y4 = ry - d * sec(Y + f /2) * tan(f /2);
                X1 = [x1, x2, x4, x3, x1];
                Y1 = [y1, y2, y4, y3, y1];
    %             d1 = (rx-x1)^2 + (ry-y1)^2;
    %             d2 = (rx-x2)^2 + (ry-y2)^2;
    %             d3 = (rx-x3)^2 + (ry-y3)^2;
    %             d4 = (rx-x4)^2 + (ry-y4)^2;
    %             measure = sum([dmax,dmax,dmax,dmax] >= [d1,d2,d3,d4]);
                if (d<dmax)
                    theta = linspace(0, 2*pi, 100);
                    xc = sqrt(dmax.^2-d.^2).*cos(theta) +rx;
                    yc = -sqrt(dmax.^2-d.^2).*sin(theta)+ry;
                    [X, Y] = polybool('intersection', X1, Y1, xc, yc);
                end
    %           if measure == 4
    %                 X = [x1, x2, x4, x3, x1];
    %                 Y = [y1, y2, y4, y3, y1];
    %             % elseif measure == 0
    %             %     X = [];
    %             %     Y = [];
    %             else
    %                 while(measure ~= 4)
    %                     if d1 > dmax & d3 > dmax
    %                         x1 = x1 + (x2-x1)*0.1;
    %                         y1 = y1 + (y2-y1)*0.1;
    %                         x3 = x3 + (x4-x3)*0.1;
    %                         y3 = y3 + (y4-y3)*0.1;
    % 
    %                     else
    %                         x2 = x2 + (x1-x2)*0.1;
    %                         y2 = y2 + (y1-y2)*0.1;
    %                         x4 = x4 + (x3-x4)*0.1;
    %                         y4 = y4 + (y3-y4)*0.1;
    % 
    %                     end
    %                     d1 = (rx-x1)^2 + (ry-y1)^2;
    %                     d2 = (rx-x2)^2 + (ry-y2)^2;
    %                     d3 = (rx-x3)^2 + (ry-y3)^2;
    %                     d4 = (rx-x4)^2 + (ry-y4)^2;
    %                     measure = sum([dmax,dmax,dmax,dmax] >= [d1,d2,d3,d4]);
    %                 end
    %                 X = [x1, x2, x4, x3, x1];
    %                 Y = [y1, y2, y4, y3, y1];
    %             end
                res = [X;Y];
           end
       end
    end
end