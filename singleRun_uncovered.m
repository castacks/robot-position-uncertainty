classdef singleRun_uncovered < parameters
    properties
        max_trials=20
    end
    methods
       function obj= singleRun_uncovered(t,varargin)
           obj=obj@parameters(varargin{:})
           if nargin>0
               obj.max_trials=t;
           end
       end
       function tspMethod(obj)
           data_check_avg = [];
            for trials = 1:obj.max_trials
                data_check = [];
                for v1 = 0.1:0.1:obj.v_max
                    v2 = v1;
                    n=1;
                    x_shift=v1/obj.fps; y_shift=v2/obj.fps;
%                     disp(['computing path for 1st run with v1 = ', num2str(v1),' and v2= ', num2str(v2)])
                    vec_x=[-x_shift/2:x_shift:(ceil(obj.lx*obj.fps/v1)+1)*x_shift];vec_y=[-y_shift/2:y_shift:(ceil(obj.ly*obj.fps/v2)+1)*y_shift];
                    x_cor=repmat(vec_x,size(vec_y,2),1);   x_cor=x_cor(:);
                    y_cor=repmat(vec_y',size(vec_x,2),1);
                    path = [ones(size(x_cor)).*-obj.d, x_cor, y_cor];
                    num_cells = ceil(obj.lx*obj.fps/v1) * ceil(obj.ly*obj.fps/v2);
                    time=[((num_cells-1)/obj.fps)];
                    vector = [1,0,0];
                    x1 = [0, 0, obj.lx, obj.lx, 0];
                    y1 = [0, obj.ly, obj.ly, 0, 0];
                    uncovered=obj.uncovered_fov_mvn(path, x1, y1, v1, v2);
                    data_check = [data_check; double(v1),double(sum(time)), double(uncovered)];
                end
                if trials == 1
                    data_check_avg = data_check;
                else
                    data_check_avg = data_check_avg + data_check;
                end

            end
                data_check_avg = data_check_avg ./20;
                filename = ['uncovered.mat'];
                save(filename, 'data_check_avg');
       end
       function uncovered_area = uncovered_fov_mvn(obj,path, x1, y1, v1, v2)  
                n = size(path, 1);
                xd = x1;
                yd = y1;
                X = [];
                Y = [];
                second_pos=[];
                error = mvnrnd(obj.mu_e, obj.sigma_value, size(path,1));

                %Check for uncovered areas by repeatedly subtracting fov polygons from
                %entire wall
                for i = 1:size(path,1)
                    pos = [path(i,1)-error(i,1), path(i,2)-error(i,2), path(i,3)-error(i,3)];
            %         pos = [path.Poses(i).Position.X, path.Poses(i).Position.Y, path.Poses(i).Position.Z];
            %         [error(i,1), error(i,2), error(i,3), error(i,4)]
                    yaw=0;
                    yaw = yaw - error(i,4)*pi/180.0;
                    res = obj.get_fov(pos(1,2),pos(1,3), yaw, -pos(1,1), obj.f);
                    x2 = res(1,:);
                    y2 = res(2,:);
                    [xd, yd] = polybool('subtraction', xd, yd, x2, y2);
                    X = [X;x2];
                    Y = [Y;y2];

                end  

            uncovered = [xd;yd];

            uncovered_area = obj.get_area(xd,yd);
            disp(['Uncovered Area ', num2str(uncovered_area)])
       end
       function uncovered_area= get_area(obj,xd,yd)
        % Find area of uncovered regions
        uncovered_area = polyarea(xd,yd);
        if isnan(uncovered_area)
            ind = find(isnan(xd));
            ind = [ind, size(xd,2)+1];
            x = xd(1:ind(1)-1);
            y = yd(1:ind(1)-1);
            uncovered_area = polyarea(x,y);
            for i = 1:size(ind,2)-1
                x = xd(ind(i)+1 : ind(i+1)-1);
                y = yd(ind(i)+1 : ind(i+1)-1);
                if ispolycw(x,y)
                    uncovered_area = uncovered_area + polyarea(x,y);
                else
                    uncovered_area = uncovered_area - polyarea(x,y);
                end
            end
        end
       end
       function res = get_fov(obj,rx, ry, Y, d, f)
            if Y >= f | Y <= -f
                X = [];
                Y = [];
            else
            dmax = 9;
            x1 = rx + d * tan(Y - f /2);
            y1 = ry + d * sec(-Y + f /2) * tan(f /2);
            x2 = rx + d * tan(Y + f /2);
            y2 = ry + d * sec(Y + f /2) * tan(f /2);
            x3 = rx + d * tan(Y - f /2);
            y3 = ry - d * sec(-Y + f /2) * tan(f /2);
            x4 = rx + d * tan(Y + f /2);
            y4 = ry - d * sec(Y + f /2) * tan(f /2);
            X1 = [x1, x2, x4, x3, x1];
            Y1 = [y1, y2, y4, y3, y1];
            d1 = (rx-x1)^2 + (ry-y1)^2;
            d2 = (rx-x2)^2 + (ry-y2)^2;
            d3 = (rx-x3)^2 + (ry-y3)^2;
            d4 = (rx-x4)^2 + (ry-y4)^2;
            measure = sum([dmax,dmax,dmax,dmax] >= [d1,d2,d3,d4]);
            if measure == 4
                X = [x1, x2, x4, x3, x1];
                Y = [y1, y2, y4, y3, y1];
            % elseif measure == 0
            %     X = [];
            %     Y = [];
            else
                while(measure ~= 4)
                    if d1 > dmax & d3 > dmax
                        x1 = x1 + (x2-x1)*0.1;
                        y1 = y1 + (y2-y1)*0.1;
                        x3 = x3 + (x4-x3)*0.1;
                        y3 = y3 + (y4-y3)*0.1;

                    else
                        x2 = x2 + (x1-x2)*0.1;
                        y2 = y2 + (y1-y2)*0.1;
                        x4 = x4 + (x3-x4)*0.1;
                        y4 = y4 + (y3-y4)*0.1;

                    end
                    d1 = (rx-x1)^2 + (ry-y1)^2;
                    d2 = (rx-x2)^2 + (ry-y2)^2;
                    d3 = (rx-x3)^2 + (ry-y3)^2;
                    d4 = (rx-x4)^2 + (ry-y4)^2;
                    measure = sum([dmax,dmax,dmax,dmax] >= [d1,d2,d3,d4]);
                end
                X = [x1, x2, x4, x3, x1];
                Y = [y1, y2, y4, y3, y1];
            end
            res = [X;Y];
            end
       end
    end
end