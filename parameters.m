classdef parameters <handle
    properties
        v_max=5;
        v_min=0.5;
        % x length of wall
        lx = 10;
        % y length of wall
        ly = 10;
        %distance from wall
        d=2;
        % Capture rate of Camera
        fps = 2
        % Field of view of Camera
        f = pi/3;
        resol=10;%1/resol is the resolution of the velocities
        r1 = 1024;
        r2 = 1024;
        sigma_value=[0.0161   -0.0145    0.0035    0.0006
           -0.0145    0.0420   -0.0089   -0.0008
            0.0035   -0.0089    0.0040    0.0001
            0.0006   -0.0008    0.0001    0.0004];
        mu_e=[0,0,0,0];
    end
    properties(Dependent)
        mu_value
    end
    methods
        function val=get.mu_value(obj)
            val=[0,0,obj.d,0]+obj.mu_e;
        end
    end
end
