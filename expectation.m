classdef expectation < parameters
   properties
       %point on the plane
       point
   end
   properties(Access=protected)
      %numerical integration parameters
      dh1=0.1 %interval for x
      dh2=0.1 %interval for y
      dh3=0.05 %interval for d
      dh4=pi/180 %interval for yaw
      %max dist for phi5
      kmax=9 
%       distribution length
      length=6
   end
  
   methods
       function thisE= expectation(P,varargin)
           thisE=thisE@parameters(varargin{:})
           if nargin>0
               thisE.point=P;
           end
       end
       function expected_value=find(thisE)
            %% exceptation 2d
            % input- p=[x,y] point on the wall 2x1
            %      - mu is mean vector 1x4
            %      - sigma is correlation matrix 4x4
            %      - f is field of view
            % output - value of expectation at that point p
            e=0.01;x=thisE.point(1,:);y=thisE.point(2,:);
            %replacing nested for loops
            dist=0:thisE.dh3:sqrt(thisE.kmax);
            rx=thisE.mu_value(1)-sqrt(thisE.kmax-thisE.d^2):thisE.dh1:thisE.mu_value(1)+sqrt(thisE.kmax-thisE.d^2);
            ry=thisE.mu_value(2)-sqrt(thisE.kmax-thisE.d^2):thisE.dh2:thisE.mu_value(2)+sqrt(thisE.kmax-thisE.d^2);
            yaw=-pi/2+thisE.f/2:thisE.dh4:pi/2-thisE.f/2;
            [RX RY D YAW]=ndgrid(rx,ry,dist,yaw);
            z1=(1+erf(-((y-RY-D.*sec(-YAW+(thisE.f/2)).*tan(thisE.f/2)).*(tan((thisE.f/2)+YAW)-tan(YAW-thisE.f/2))-(tan(thisE.f/2).*(sec(YAW+thisE.f/2)-sec(-YAW+thisE.f/2))).*(x-RX-D.*tan(YAW-thisE.f/2)))./e))./2;
            z2=(1+erf(-(x-RX-D.*tan(YAW+thisE.f/2) )./e))./2;
            z3=(1+erf(((y-RY+D.*sec(-YAW+(thisE.f/2)).*tan(thisE.f/2)).*(tan((thisE.f/2)+YAW)-tan(YAW-thisE.f/2))-(tan(thisE.f/2).*(-sec(YAW+thisE.f/2)+sec(-YAW+thisE.f/2))).*(x-RX-D.*tan(YAW-thisE.f/2)))/e))/2;
            z4=(1+erf((x-RX-D.*tan(YAW-thisE.f/2))/e))/2;
            z5=(1+erf(-((x-RX).^2+(y-RY).^2+D.^2-thisE.kmax)/e))/2;
            z=z1.*z2.*z3.*z4.*z5;
            r=[RX(:) RY(:) D(:) YAW(:)];
            prob=mvnpdf(r,thisE.mu_value,thisE.sigma_value);
            temp=z.*reshape(prob,length(rx),length(ry),length(dist),length(yaw)).*(thisE.dh1*thisE.dh2*thisE.dh3*thisE.dh4);
            expected_value=sum(temp(:));
       end
       function pdf=distribution(thisE)
                pdf=zeros((thisE.length*thisE.resol)+1,(thisE.length*thisE.resol)+1);
                for i=-(thisE.length/2):1/thisE.resol:(thisE.length/2)
                    disp(['x=',num2str(i)])
                    for j=-(thisE.length/2):1/thisE.resol:(thisE.length/2)
                        thisE.point=[i;j];
                        pdf(int16((i+(thisE.length/2))*thisE.resol+1),int16((j+(thisE.length/2))*thisE.resol+1))=thisE.find();
                    end
                end
                filename = ['results/2016_11_20/result_',num2str(thisE.d),'.mat']
                save(filename,'pdf')
           end
      end      
end