### What is this repository for? ###
This repository is for finding optimal velocities to traverse while inspecting planar regions given the camera parameters and robot execution uncertainty.

### How do I get set up? ###

Clone the repo into workspace,

To find no of runs and velocities in each run:

```
#!matlab
load high_res_0_5_dist_1_5;

[n,t,v]=find_no_of_runs(result, [l_x,l_y])
```
To find the overall probability of success for a given robot execution uncertainty (sigma) and camera field of view (f), for a plane of l_x and l_y dimensions

run Test4d.m (it will take more time to compute)

you can change those variables in that file..

It uses expectation4d_new function which is vectorized version of expectation4d 
which takes array of points on the wall and outputs the expectation of those points being covered.

Part 2:

to generate waypoints and publish to ros, run publish_path.m by loading desired results

to generate second run based on path followed in the first run, run publish_second_path.m by either subscribing to ros message of path followed in the first run or by loading results results/PoseArray_1_5.mat

Several expectation distributions are saved in workspaces named high_res_(sigma)_dist_(distance from wall).mat

### Dependencies ###
1. Ortools for Python

To Download Ortools, download tar.gz or zip file for Python and follow the instructions given here:
https://developers.google.com/optimization/installing
### Who do I talk to? ###

* srikanth malla (mallasrikanth004@gmail.com)
* Swetha Mandava (sweth.mandava@gmail.com)

### License ###
[This software is BSD licensed.](http://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2015, Carnegie Mellon University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.