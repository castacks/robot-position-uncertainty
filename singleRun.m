classdef singleRun < parameters & handle
    properties
        results_file ;
        threshold=0.999%for single run
        upper_bound_time
        optimal_value
        result
    end
    methods
        function obj=singleRun(resultFile,varargin)
            obj=obj@parameters(varargin{:});
            if(nargin>0)
                obj.results_file=resultFile;
            end
        end
        function res= velcheck(obj)
            load(obj.results_file)
            pdf = pdf(:,:);
            result=[];
            obj.resol=10;
            for vel_x=obj.v_min:0.1:obj.v_max
                vel_y = vel_x;
                d_x=(size(pdf,1)-1)/(obj.resol);
                d_y=(size(pdf,2)-1)/(obj.resol); %distribution x and y lengths
                n_x=(int32(d_x/(vel_x/obj.fps))-1)*2;% no of near waypoints effecting in x
                n_y=(int32(d_y/(vel_y/obj.fps))-1)*2;% no of near waypoints effecting in y
                x_shift=vel_x/obj.fps;
                y_shift=vel_y/obj.fps;
                len_x=[];len_y=[];%array of lengths of near obj.distrib that effect current obj.distrib
                %find p1
                if ((x_shift>=d_x)|(y_shift>=d_y))
                    error('too fast')
                end
                p1=pdf(int32((d_y-y_shift)*obj.resol/2):int32((d_y+y_shift)*obj.resol/2),int32((d_x-x_shift)*obj.resol/2):int32((d_x+x_shift)*obj.resol/2));
                for i=1:(n_x/2)%since computing with both sides waypoints at a time..
                    temp=int32(((d_x-x_shift)/2)*obj.resol-((i-1)*x_shift)*obj.resol);
                    len_x=[len_x;temp];
                    if temp>size(p1,2)
                        temp1=temp-size(p1,2)+1;
                        p2=pdf(int32((d_y-y_shift)*obj.resol/2):int32((d_y+y_shift)*obj.resol/2),temp1:temp);
                        p1=p1+p2-p1.*p2;
                        p2=pdf(int32((d_y-y_shift)*obj.resol/2):int32((d_y+y_shift)*obj.resol/2),size(pdf,2)-temp:size(pdf,2)-temp1);
                        p1=p1+p2-p1.*p2;
                    elseif temp<=0
                        continue
                    else 
                        p2=pdf(int32((d_y-y_shift)*obj.resol/2):int32((d_y+y_shift)*obj.resol/2),1:temp);
                        p1(:,size(p1,2)-temp+1:size(p1,2))=p1(:,size(p1,2)-temp+1:size(p1,2))+p2-p1(:,size(p1,2)-temp+1:size(p1,2)).*p2;
                        p2=pdf(int32((d_y-y_shift)*obj.resol/2):int32((d_y+y_shift)*obj.resol/2),size(pdf,2)-temp+1:size(pdf,2));
                        p1(:,1:temp)=p1(:,1:temp)+p2-p1(:,1:temp).*p2;
                    end
                end
                for i=1:(n_y/2) %since computing with both sides waypoints at a time..
                    temp=int32(((d_y-y_shift)/2)*obj.resol-((i-1)*y_shift)*obj.resol);
                    len_y=[len_y;temp];
                    if temp>size(p1,1)
                        temp1=temp-size(p1,1)+1;
                         p2=pdf(temp1:temp,int32((d_x-x_shift)*obj.resol/2):int32((d_x+x_shift)*obj.resol/2));
                         p1=p1+p2-p1.*p2;
                         p2=pdf(size(pdf,2)-temp:size(pdf,2)-temp1,int32((d_x-x_shift)*obj.resol/2):int32((d_x+x_shift)*obj.resol/2));
                         p1=p1+p2-p1.*p2;
                    elseif temp<=0
                        continue
                    else
                         p2=pdf(1:temp,int32((d_x-x_shift)*obj.resol/2):int32((d_x+x_shift)*obj.resol/2));
                         p1(size(p1,1)-temp+1:size(p1,1),:)=p1(size(p1,1)-temp+1:size(p1,1),:)+p2-p1(size(p1,1)-temp+1:size(p1,1),:).*p2;
                         p2=pdf(size(pdf,2)-temp+1:size(pdf,2),int32((d_x-x_shift)*obj.resol/2):int32((d_x+x_shift)*obj.resol/2));
                         p1(1:temp,:)=p1(1:temp,:)+p2-p1(1:temp,:).*p2;
                    end
                end    
                z1 = ceil(obj.lx/x_shift);%total no of wp in x
                z2 = ceil(obj.ly/y_shift);%total no of wp in y
                z=int32(z1*z2);        
                len_y=obj.ly;
                len_x=len_y./(vel_y/obj.fps).* obj.lx;
                time=len_x/vel_x + len_y/vel_y;
                prob = prod(p1(:));
                [L, n] = obj.expected_trials(p1, vel_x);
                result=[result;double(obj.d),double(vel_x),double(vel_y), double(time), double(prob^double(z)), double(L)./vel_x, double(n)];
            end
            [ind1]=find(result(:,5) > obj.threshold);
%             ind1 = find(result(ind1,4) == min(result(ind1,4)));
            [m_t,ind2]=min(result(ind1,4));
            
            res=result(ind1(ind2),1:4);
            obj.optimal_value=res;
            obj.upper_bound_time=result(:,6);
            obj.result=result;
        end
       function [L,n] = expected_trials(obj,p1, v)
            p1(:) = 1-p1(:);
            p2 = circshift(p1,[floor(size(p1,1)/2),floor(size(p1,2)/2)]);
            n=1;
            prob = p1;
            num_cells = ceil(obj.lx*obj.fps/v) * ceil(obj.ly*obj.fps/v);
            l = (num_cells-1)*(v/obj.fps);
            unc_area=sum(prob(:))*num_cells;
%             L=l+unc_area*l;
            L = l + unc_area*l;
            while(unc_area>0.1)
                n = n+1;
                if n == 1000
                    break
                end
                if rem(n,2) == 0
                    prob = prob.*p2;
                else
                    prob = prob.*p1;
                end
                unc_area=sum(prob(:))*num_cells;
                L=L+unc_area*l;
            end
        end

%         function [L,n] = expected_trials(obj,p1, v)
%             p1(:) = 1-p1(:);
%             p2 = circshift(p1,[floor(size(p1,1)/2),floor(size(p1,2)/2)]);
%             n=1;
%             prob =p1;
%             num_cells = ceil(obj.lx*obj.fps/v) * ceil(obj.ly*obj.fps/v);
%             l = (num_cells-1)*(v/obj.fps);
%             unc_area=sum(prob(:))./size(prob(:),1);L=l+unc_area*l;
%             while(unc_area>0.00001)
%                 n = n+1;
%                 if n == 1000
%                     break
%                 end
%                 if rem(n,2) == 0
%                     prob = prob.*p2;
%                 else
%                     prob = prob.*p1;
%                 end
%                 unc_area=sum(prob(:))./size(prob(:),1);
%                 L=L+unc_area*l;
%             end
%         end
        
        
    end
end
